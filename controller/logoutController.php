<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/4/15
 * Time: 4:23 PM
 */

class logoutController extends siteController {
    function __construct(){
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        \Emagid\Core\Membership::destroyAuthenticationSession();
        redirect('/');
    }
}