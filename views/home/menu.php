<a href="/"><div class='back_home'>
    <img src="<?= FRONT_ASSETS ?>img/home.png">
</div></a>

<section class='menu_page content_page'>
    <div class='head'>
        <h1>MENU</h1>
        <p class='desc'>Dine with us</p>
        <img src="<?= FRONT_ASSETS ?>img/star.png">
    </div>


    <div class='menus'>
        <div class='menu'>
           <p class='name'>PAN CON TOMATE<span>6</span></p> 
           <p class='desc'>Grilled Stirato bread rubbed with tomato, garlic and olive oil</p>
        </div>
        <div class='menu'>
           <p class='name'>PIMIENTOS DE PADRÓN<span>9</span></p> 
           <p class='desc'>Blistered Shishito peppers, coarse sea salt</p>
        </div>
        <div class='menu'>
           <p class='name'>COCA DE SETAS<span>15</span></p> 
           <p class='desc'>Grilled flatbread, porcini purée, Idiazábal cheese, caramelized onions, wild mushrooms, pine nuts, arugula</p>
        </div>
        <div class='menu'>
           <p class='name'>ESPINACAS A LA CATALANA<span>12</span></p> 
           <p class='desc'>Sautéed spinach, garbanzos, pine nuts, garlic, golden raisins</p>
        </div>
        <div class='menu'>
           <p class='name'>ENSALADA ROMANA <span>11</span></p> 
           <p class='desc'>Romaine hearts, romesco, Idiazábal cheese, mint, caramelized hazelnuts</p>
        </div>
        <div class='menu'>
           <p class='name'>TORTILLA ESPAÑOLA<span>8</span></p> 
           <p class='desc'>Traditional Spanish omelet of organic eggs, con t potatoes, Spanish onions</p>
        </div>
        <div class='menu'>
           <p class='name'>ESCALIVADA<span>12</span></p> 
           <p class='desc'>Fire roasted eggplant, red pepper, onion, labneh yogurt, fresh herbs and olive oil, served with flatbread</p>
        </div>
        <div class='menu'>
           <p class='name'>PATATAS BRAVAS<span>10</span></p> 
           <p class='desc'>Crispy potatoes, salsa brava, roasted garlic aioli</p>
        </div>
    </div>
</section>