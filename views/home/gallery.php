<a href="/"><div class='back_home'>
    <img src="<?= FRONT_ASSETS ?>img/home.png">
</div></a>

<section class='menu_page'>
    <div class='head'>
        <h1>GALLERY</h1>
        <p class='desc'>Experience what makes us great</p>
        <img src="<?= FRONT_ASSETS ?>img/star.png">
    </div>

    <div class='pics'>
        <img src='<?= FRONT_ASSETS ?>img/social1.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social2.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social3.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social4.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social5.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social6.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social7.jpg' class='pic'>
        <img src='<?= FRONT_ASSETS ?>img/social8.jpg' class='pic'>
    </div>
</section>

<script type="text/javascript">
	$('.pics').masonry({
	  // options
	  itemSelector: '.pic'
  	});
	// layout Masonry after each image loads
	$('.pics').imagesLoaded().progress( function() {
	  $('.pics').masonry();
	}); 
</script>