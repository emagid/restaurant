<?php

namespace Model;

class Contact extends \Emagid\Core\Model {
    static $tablename = "public.contact";

    public static $fields  =  [
        'name',
        'email',
        'phone',
        'message',
        'form',
        'donation'
    ];

    public static $forms = [
        1=>'Share Picture',
        2=>'Share Gif',
        3=>'Donate'
    ];

}
























